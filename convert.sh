#!/bin/bash
for d in ./week_*; do
  cd "$d" || exit 1
  for f in *.R; do
    jupytext --to Rmd "$f"
    jupytext --to notebook "$f"
  done
  cd ..
done
