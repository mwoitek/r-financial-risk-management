# Estimating VaR of the Normal Distribution


## Setup

```{r}
suppressPackageStartupMessages(library(quantmod))
load(file = "FRED_gold.rda")
```

```{r}
log_ret <- diff(log(gold))[-1]
```

```{r}
mu <- mean(log_ret)
sig <- sd(log_ret)
```

## Question 1

What is the value-at-risk (VaR) of the daily log returns in gold, at the
95% confidence level?

Enter the answer using six decimal places, i.e., n.nnnnnn (where n is an
integer). If this is a negative number, please add a minus ("-") sign in
front. If this is a positive number, there is no need to add a plus sign
in front.

```{r}
var <- qnorm(0.05, mean = mu, sd = sig)
round(var, digits = 6)
# -0.019763
```

## Question 2

Suppose the hedge fund invested the entire $1000 million in gold instead
of US equities. What is the VaR of the daily changes in the assets of the
hedge fund, at the 95% confidence level?

Enter the answer in terms of millions of US Dollars, using one decimal
place, i.e., nnn.n (where n is an integer). If this is a negative number,
please add a minus ("-") sign in front. If this is a positive number,
there is no need to add a plus sign in front.

```{r}
round(1000 * (exp(var) - 1), digits = 1)
# -19.6
```
