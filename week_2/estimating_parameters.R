# %% [markdown]
# # Estimating Parameters of the Normal Distribution

# %% [markdown]
# ## Loading data

# %%
suppressPackageStartupMessages(library(quantmod))
load(file = "FRED_gold.rda")

# %% [markdown]
# ## Question 1
#
# What is the estimated mean of the daily log returns in gold?
#
# Enter the answer using eight decimal places, i.e., n.nnnnnnnn (where n is
# an integer).

# %%
log_ret <- diff(log(gold))[-1]

# %%
format(mean(log_ret), nsmall = 8, scientific = FALSE)
# 0.00008776413

# %% [markdown]
# ## Question 2
#
# What is the estimated standard deviation of the daily log returns in
# gold?
#
# Enter the answer using six decimal places, i.e., n.nnnnnn (where n is an
# integer).

# %%
round(sd(log_ret), digits = 6)
# 0.012069
