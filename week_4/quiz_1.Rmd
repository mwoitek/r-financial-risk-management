# Week 4 Quiz (1 of 4)


## Setup

```{r}
suppressPackageStartupMessages(library(quantmod))
suppressPackageStartupMessages(library(rugarch))
```

```{r}
getSymbols("DEXJPUS", src = "FRED")
dexjpus <- na.omit(DEXJPUS)
dexjpus <- dexjpus["1979-12-31/2017-12-31"]
dexjpus <- 1 / dexjpus
names(dexjpus) <- "TR"
log_ret <- diff(log(dexjpus))[-1]
```

## Question 1

Does the acf of log returns show strong evidence of serial correlation?

Enter "YES" if there is strong evidence, and "NO" if there is no strong
evidence of serial correlation.

```{r}
acf(log_ret)
# NO
```

## Question 2

Does the acf of |log returns| show strong evidence of volatility clustering?

Enter "YES" if there is strong evidence, and "NO" if there is no strong
evidence of serial correlation.

```{r}
acf(abs(log_ret))
# YES
```

## Question 3

Next, estimate the GARCH(1, 1) - t model, and answer the following questions:

What is the estimated $\alpha_{1}$ ("alpha1") parameter of the GARCH model?

Enter the answer using six digits, i.e., nnnnnn (where n is an integer). If
this is a negative number, please add a minus ("-") sign in front. If this is
a positive number, there is no need to add a plus sign in front.

```{r}
uspec <- ugarchspec(
  variance.model = list(model = "sGARCH", garchOrder = c(1, 1)),
  mean.model = list(armaOrder = c(0, 0), include.mean = TRUE),
  distribution.model = "std"
)
fit_garch <- ugarchfit(spec = uspec, data = log_ret[, 1])
garch_params <- fit_garch@fit$coef
```

```{r}
# This answer is considered wrong. But it is actually correct. I know that
# because the exact same code produces the right answer for version 2 of
# this quiz.
round(garch_params["alpha1"], digits = 6)
#   alpha1
# 0.045047
```

## Question 4

What is the estimated $\beta_{1}$ ("beta1") parameter of the GARCH model?

Enter the answer using six digits, i.e., nnnnnn (where n is an integer). If
this is a negative number, please add a minus ("-") sign in front. If this is
a positive number, there is no need to add a plus sign in front.

```{r}
round(garch_params["beta1"], digits = 6)
#    beta1
# 0.944748
```

## Question 5

What is the estimated $\nu$ ("shape") parameter of the GARCH model?

Enter the answer using six digits, i.e., nnnnnn (where n is an integer). If
this is a negative number, please add a minus ("-") sign in front. If this is
a positive number, there is no need to add a plus sign in front.

```{r}
# This answer is considered wrong. But it is actually correct. I know that
# because the exact same code produces the right answer for version 2 of
# this quiz.
round(garch_params["shape"], digits = 6)
# 4.954720
```

## Question 6

Set the seed to 123789. If you are using R version 3.6.0 or higher, please
add the following command BEFORE setting the seed value: `RNGkind(sample.kind
= "Rounding")`

Use the `ugarchboot` function to simulate 100,000 outcomes for 1-day ahead.

Then answer the following questions:

What is the VaR at the 95% confidence level for the exchange rate?

Enter the answer using six digits, i.e., nnnnnn (where n is an integer). If
this is a negative number, please add a minus ("-") sign in front. If this is
a positive number, there is no need to add a plus sign in front.

```{r}
suppressWarnings(RNGkind(sample.kind = "Rounding"))
set.seed(123789)
boot_garch <- ugarchboot(
  fit_garch,
  method = "Partial",
  sampling = "raw",
  n.ahead = 1,
  n.bootpred = 100000
)

rvec <- boot_garch@fseries
alpha <- 0.05
var <- quantile(rvec, alpha)
round(var, digits = 6)
#        5%
# -0.006993
```

## Question 7

What is the ES at the 95% confidence level for the exchange rate?

Enter the answer using six digits, i.e., nnnnnn (where n is an integer). If
this is a negative number, please add a minus ("-") sign in front. If this is
a positive number, there is no need to add a plus sign in front.

```{r}
es <- mean(rvec[rvec < var])
round(es, digits = 6)
# [1] -0.009603
```
