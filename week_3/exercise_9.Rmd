# Skewness, Kurtosis, Jarque-Bera Test for Normality


## Setup

```{r}
suppressPackageStartupMessages(library(moments))
suppressPackageStartupMessages(library(quantmod))
```

```{r}
load(file = "FRED_gold.rda")
log_ret_gold <- diff(log(gold))[-1]
```

```{r}
getSymbols("WILL5000IND", src = "FRED")
wilsh <- na.omit(WILL5000IND)
wilsh <- wilsh["1979-12-31/2017-12-31"]
names(wilsh) <- "TR"
log_ret_wilsh <- diff(log(wilsh))[-1]
```

## Question 1

What is the skewness coefficient of the daily log returns in gold?

Enter the answer using two decimal places, i.e., n.nn (where n is an
integer). If this is a negative number, please add a minus ("-") sign in
front. If this is a positive number, there is no need to add a plus sign
in front.

```{r}
skewness_gold <- skewness(as.vector(log_ret_gold))
round(skewness_gold, digits = 2)
# [1] -0.09
```

## Question 2

Is the daily log return of Gold more, or less, left skewed than that of
the Wilshire 5000 index?

Select "MORE" if Gold is more left skewed than the Wilshire 5000 index.
Select "LESS" if Gold is less left skewed than the Wilshire 5000 index.

```{r}
skewness_wilsh <- skewness(as.vector(log_ret_wilsh))
round(skewness_wilsh, digits = 2)
# [1] -0.91
```

```{r}
if (abs(skewness_gold) > abs(skewness_wilsh)) {
  print("MORE")
} else {
  print("LESS")
}
# [1] "LESS"
```

## Question 3

What is the kurtosis coefficient of the daily log returns in gold?

Enter the answer using two decimal places, i.e., nn.nn (where n is an
integer). If this is a negative number, please add a minus ("-") sign in
front. If this is a positive number, there is no need to add a plus sign
in front.

```{r}
kurtosis_gold <- kurtosis(as.vector(log_ret_gold))
round(kurtosis_gold, digits = 2)
# [1] 15.43
```

## Question 4

Is the daily log return of Gold more, or less, heavier tailed than that
of the Wilshire 5000 index?

```{r}
kurtosis_wilsh <- kurtosis(as.vector(log_ret_wilsh))
round(kurtosis_wilsh, digits = 2)
# [1] 21.8
```

```{r}
if (kurtosis_gold > kurtosis_wilsh) {
  print("MORE")
} else {
  print("LESS")
}
# [1] "LESS"
```

## Question 5

Run the Jarque-Bera test of normality for the daily log returns in gold.
What is the JB test statistic?

Enter this as a six digit number, i.e., nnnnnn (where n is an integer).
If this is a negative number, please add a minus ("-") sign in front. If
this is a positive number, there is no need to add a plus sign in front.

```{r}
test_res <- jarque.test(as.vector(log_ret_gold))
stringr::str_pad(round(test_res$statistic), 6, pad = "0")
# [1] "061330"
```

## Question 6

Does the JB test reject normality of the daily log returns in gold at the
1% significance level?

Select "YES" if the JB test rejects normality, and "NO" if the JB test
does not reject normality.

```{r}
test_res
# JB = 61330, p-value < 2.2e-16
# YES
```
